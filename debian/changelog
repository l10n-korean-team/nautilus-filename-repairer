nautilus-filename-repairer (0.2.0-3) unstable; urgency=low

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update standards version to 4.4.1, no changes needed.

  [ Changwoo Ryu ]
  * debian/control: Update to debhelper-compat (= 13)
  * debian/copyright: Update copyright year
  * debian/control: Remove autotools-dev from B-D
  * debian/control: Add 'Rules-Requires-Root: no'
  * Bump Standard-Version to 4.5.0

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 25 May 2020 17:08:33 +0900

nautilus-filename-repairer (0.2.0-2) unstable; urgency=medium

  [ Changwoo Ryu ]
  * debian/control:
    - Update Vcs-* to the new salsa.d.o repository
    - Standards-Version: 4.4.0
  * d/salsa-ci.yml: Add Salsa CI config
  * d/p/update-translation-es.patch: Update Spanish translation (Closes:
    #807501) by Manuel "Venturi" Porras Peralta <venturi@openmailbox.org>

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 19 Jul 2019 22:46:43 -0300

nautilus-filename-repairer (0.2.0-1) unstable; urgency=medium

  [ Changwoo Ryu ]
  * New Upstream Version
  * debian/{copyright,control,watch}: Update the upstream URL
  * debian/copyright: Correct DEP-5
  * debian/rules: Add hardening option
  * debian/control: Standards-Version: 3.9.8
  * debian/rules: Avoid useless library dependencies

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 11 Feb 2017 21:29:21 +0900

nautilus-filename-repairer (0.1.1-4) unstable; urgency=low

  [ Changwoo Ryu ]
  * Add German translation (Closes: #636375)
  * Standards-Version: 3.9.5
  * debian/control: Correct Vcs-* fields

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 27 Jan 2014 03:41:30 +0900

nautilus-filename-repairer (0.1.1-3) unstable; urgency=low

  [ Changwoo Ryu ]
  * Clean up debian/watch
  * Use debhelper 9
  * Standards-Version: 3.9.4
  * Use the copyright format 1.0
  * Remove unneeded build-dependency on quilt
  * Add DEP-3 tags on patch
  * Add -as-needed ld option
  * Add git-buildpackage config

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 02 Feb 2013 22:02:55 +0900

nautilus-filename-repairer (0.1.1-2) unstable; urgency=low

  [ Changwoo Ryu ]
  * debian/control:
    - Explicitly build-depends on libnautilus-extension-dev 3.x

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 15 Oct 2011 00:34:04 +0900

nautilus-filename-repairer (0.1.1-1) unstable; urgency=low

  * New Upstream Version
  * Standards-Version: 3.9.2
  * Build both for GNOME2 and GNOME3 nautilus

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 13 Aug 2011 17:45:09 +0900

nautilus-filename-repairer (0.0.6-1+gnome3+3) experimental; urgency=low

  * Build-Depends on intltool (Closes: #619206)

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 23 Mar 2011 03:07:17 +0900

nautilus-filename-repairer (0.0.6-1+gnome3+2) experimental; urgency=low

  * Build-Depends on dh-autoreconf (Closes: #619124)

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 21 Mar 2011 23:25:45 +0900

nautilus-filename-repairer (0.0.6-1+gnome3+1) experimental; urgency=low

  * debian/watch: now works with the recent Google Code update
  * Use source format 3.0 (quilt)
  * Drop CDBS and use debhelper 7.
  * Standards-Version: 3.9.1
  * Build for Nautilus 3

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 21 Mar 2011 05:37:47 +0900

nautilus-filename-repairer (0.0.6-1) unstable; urgency=low

  * New Upstream Version (Closes: #559623)
  * New upstream project homepage (Closes: #559465)
  * Standards-Version: 3.8.3

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 18 Jan 2010 02:59:19 +0900

nautilus-filename-repairer (0.0.5-3) unstable; urgency=low

  * Upload to unstable. (Closes: #529871)
  * Standards-Version: 3.8.2

 -- Changwoo Ryu <cwryu@debian.org>  Thu, 30 Jul 2009 23:46:15 +0900

nautilus-filename-repairer (0.0.5-2) experimental; urgency=low

  * Used -X option to dh_makeshlibs.
  * Build-Depends on libnautilus-extension-dev (>= 2.22.0) for the new
    nautilus extension ABI. (Closes: #475232)

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 01 Apr 2008 22:54:07 +0900

nautilus-filename-repairer (0.0.5-1) unstable; urgency=low

  * New Upstream Version
  * Prevent useless call to ldconfig in postinst/postrm, by appending
    --noscripts option to dh_makeshlibs.

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 01 Apr 2008 22:11:57 +0900

nautilus-filename-repairer (0.0.4-1) unstable; urgency=low

  * New Upstream Version

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 10 Mar 2008 19:13:09 +0900

nautilus-filename-repairer (0.0.3-1) unstable; urgency=low

  * Initial release (Closes: #463667)

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 06 Feb 2008 16:02:55 +0900
